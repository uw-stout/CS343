# Project 2: 3D modeling and camera controls (120 points)

To get comfortable with 3D graphics, in this assignment you will create a simple model in Blender and import it into openFrameworks.  Once you have the model loaded into openFrameworks, you will implement a camera system for navigating the environment. 

I’ll repeat the list of reminders for working with openFrameworks from the previous assignment:
* openFrameworks needs to be installed on the same hard drive as the Visual Studio projects.
* It’s safe to upgrade a project created by the openFrameworks project generator to Visual Studio 2019 (default upgrade settings are fine).
* You do need to switch your build configuration from Win32 to x64 (to avoid a FreeImage error).
* One you create a project, don’t move it to another directory or the relative path to openFrameworks will break.
* C++ source files need to go in the “src” folder for an openFrameworks project.  This is not the Visual Studio default.  If you create your C++ files from within Solution Explorer, make sure you specify “<your solution name>/src” as the directory.
* Similarly, shader files need to go in “bin/data.”
* To get GLSL syntax highlighting, download the following Visual Studio extension: https://marketplace.visualstudio.com/items?itemName=DanielScherzer.GLSL
* GLSL syntax errors will be displayed in the “Output” panel at runtime (not in the “Error List” or in the console).
* Make sure that your images load successfully:
```assert(img.getWidth() != 0 && img.getHeight() != 0);```
* Don’t forget to modify ```main()``` (in main.cpp) to specify the OpenGL version (4.1).

## Early check-in (20 points)
For this project, you are required to complete an early check-in.  The early check-in is worth 20 points and will be evaluated as follows:
* 10 points for checking in regardless of where you’re at (even if it doesn’t work at all).
* 10 points for making substantial, visible progress on Part 1.

I can check you off either during class time or in office hours.  **You should also add me (@tetzlaff) as a member with "Reporter"-level (read-only) access to the repository and submit the URL to Canvas for the "Project 2 check-in" assignment.**

## Part 1 (25 points): 3D modeling in Blender
Download the latest version of Blender from: https://www.blender.org/

Create a new Blender project and make a scene composed of several primitive objects.  Use translation, rotation and scale to position the objects where you want in your 3D scene.  Each partner should do this independently (although you’re encouraged to help each other learn how to use Blender), so you should have two different Blender scenes when you’re done.

Once you’ve finished creating your scenes in Blender, export them in the .ply format.  Add these models to the bin/data directory for project 2 in your repository.

Now, open your openFrameworks project (project2.sln), load each one of your models as an ofMesh, and write the necessary code to render the models together on screen.  Apply an appropriate model transformation to each so that the models aren’t on top of / intersecting with each other.  **Each model should have a different model transformation.**  Try to have the models centered in Blender when you export them so that the transformation roughly corresponds to the position, scale, and orientation of the model in your scene.  Render the scene in perspective projection in preparation for part 2.  

For your fragment shader, write a normal visualization shader like the one used in lecture.  The shader should **transform the normals into world space** using a normal matrix.

## Part 2 (25 points): Interactive camera controls
To receive full credit for this assignment, you need to add first-person “flying camera” controls to your project.  You must support the following camera controls:
* Movement in six directions: forwards, backwards, left, right, up, and down.  These directions are defined relative to the camera’s orientation, for example, “forward” means “in the direction the camera is facing,” taking into account head and pitch.
* Rotation controls, including head and pitch for the camera as defined in the Euler transformation.  (Roll is not required.)  Use the mouse to control head and pitch.

## Part 3 (25 points): VBO optimization and stress testing
Once you have interactive controls, you should stress test your program by drawing a scene with several thousand models (copies of the two models you created for part 1).  Then switch from drawing ofMesh directly to initializing an ofVbo in ```setup()``` and drawing the VBO in ```draw()```.  You should notice a considerable performance boost when making this optimization.  **To receive full credit, include a comment in your source code** describing the outcome of this optimization test.

## Part 4 (25 points): Alpha blending fog
Once of the simplest ways to implement fog in a 3D video game is to use alpha blending, where the alpha value decreases as the distance from the camera increases.

Here is a suggested method for doing this:
* Pass the model-view matrix (```view * model```, no projection) as a uniform to your vertex shader.
* Calculate the camera space position in the vertex shader and pass that to the fragment shader.
* Calculate the distance from the camera as the length of the camera space position vector.

Compute an alpha value that goes to zero as the distance approaches the far plane.  Hint: Consider using the built-in GLSL function ```smoothstep()```.

This will look better if you add the command: ```glEnable(GL_CULL_FACE);``` to ```ofApp::setup()```.  This will ensure that that back-facing polygons do not become visible due to blending.

## Just for fun
As you work on this assignment, there are a few additional things that you can do for fun if you’re interested.  No extra credit will be awarded for any of these (and you won’t lose points for not having them), but they should be fairly easy to add once everything else works.
* You may want to disable the cursor and allow the cursor a wider range of motion than would normally be permitted by the screen resolution.  
    * There’s a function from GLFW (a window manager that is usually managed for us by openFrameworks) to accomplish this:
    ```c++
    auto window { ofGetCurrentWindow() };
    glfwSetInputMode(dynamic_pointer_cast<ofAppGLFWWindow>(window)->getGLFWWindow(), 
        GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    ```
    * For the GLFW function to be available, you’ll also need to ```#include "GLFW/glfw3.h"```.  
    * It will be easier to debug the project if you don’t disable the cursor until everything is working.  
* You can run the app in full-screen mode by adding the following line to main.cpp:
```window->setFullscreen(true);```
You shouldn’t do this until after you have your project working to make debugging easier.

 
## Submission
Make sure that all of the following are committed to your Git repository in the appropriate directory:
* Your Blender projects **and** the PLY files exported from Blender in bin/data.
* main.cpp, ofApp.h, and ofApp.cpp
* Your vertex and fragment shaders
* Any other .h or .cpp files that you created.

**To indicate that all files have been committed and pushed and that your project is ready for grading, submit the URL of the repo to Canvas for the full "Project 2" assignment. (even if it's the same URL as for past projects and/or the check-in for this project).**
