# Project 3: 3D terrain rendering (120 points)

In computer games and other 3D simulations, 2D images called heightmaps are often used to represent terrain.  In order to be rendered to the screen, these heightmaps need to be converted into triangles.  In addition, for large open worlds, it is often necessary to represent the terrain at multiple levels of detail for performance purposes.

In this assignment, you will write a function that converts a heightmap into a terrain mesh, and create rendering code and a shader to display this mesh with directional and ambient light.  For full credit, you will need to also render a second terrain mesh at a higher level of detail when the camera gets close.

It is recommended, but not required, that you copy over code from the previous project as a starting point for this project.  **If you don’t use project 2 as a starting point, it is required that you still provide equivalent camera controls** (camera pitch / head; move forwards / backwards / left / right, and some kind of vertical motion, which could be jumping).  Ask your instructor for a solution if you aren’t satisfied with where you ended up on the last project.

Two heightmap options are included in the starter repository:
* A fan-made heightmap of the world of Tamriel (from the Elder Scrolls video game series), (Original URL: https://www.nexusmods.com/skyrim/mods/52675)  
* A procedurally generated noise image (created in Substance Designer) that can be used effectively as a heightmap.

Feel free to look for other heightmaps that interest you, but be aware that there may be some format differences that you’ll need to account for in your code.  Come to office hours if you want some help loading the heightmap of your choice. 

The repository for this project includes some additional supplemental files, including several classes for handling optional tasks that would be otherwise out-of-scope for this project:
* The first part of this assignment can be completed by writing a specific function called ```buildTerrainMesh()```, declared in buildTerrainMesh.h.  Several of the optional classes rely on a correct implementation of this function in order to work properly.  Alternatively, you can just call this function explicitly to make a single terrain mesh.
* A ```CellManager``` class template has been provided which is able to automatically load and unload regions of the map by calling ```buildTerrainMesh()``` when appropriate.  Use of this class is necessary for part 2 of the project.
* A class called ```World``` has been provided for storing the heightmap along with some configuration parameters.  Use of this class is optional.
* A ```CharacterPhysics``` class has been provided if you want to make the camera / character be affected by gravity.  Use of this class is optional.

## Early check-in (20 points)
The early check-in is worth 20 points and will be evaluated as follows:
* 10 points for anything showing me where you’re at (even if it doesn’t work at all).
* 10 points for making substantial, visible progress on Part 1.

I can check you off either during class time or in office hours.  **You should also add me (@tetzlaff) as a member with "Reporter"-level (read-only) access to the repository and submit the URL to Canvas for the "Project 3 check-in" assignment.**

## Part 1 (50 points): Building and rendering a terrain mesh
Open project3.sln and find buildTerrainMesh.h.  For the first part of this project, you need to:
* Complete the implementation for the ```buildTerrainMesh``` function declared in buildTerrainMesh.h.  (You should create a file called buildTerrainMesh.cpp.)
* Initialize and draw the terrain mesh in ```ofApp```.
* Integrate the camera controls from the previous project so that you can move around the world.  There are a few optional things you can do to make this more effective that are described at the end of the instructions (but no extra credit).
### Implementing buildTerrainMesh
```buildTerrainMesh()``` is a function that should contain the logic of assembling the terrain mesh.
```c++
void buildTerrainMesh(ofMesh& terrainMesh, const ofShortPixels& heightmap, 
    unsigned int xStart, unsigned int yStart, unsigned int xEnd, unsigned int yEnd, glm::vec3 scale);
```
The parameters are as follows:
* ```terrainMesh``` is a reference to the mesh that needs to be initialized.
* ```heightmap``` is a reference to the heightmap pixels that define the shape of the mesh.
* ```xStart```, ```yStart```, ```xEnd```, and ```yEnd``` define the rectangle within the heightmap that will be included in the mesh.  These are inclusive bounds, so you should assume that there will be a vertex at (```xStart```, ```yStart```) as well as a vertex at (```xEnd```, ```yEnd```).
* ```scale``` is a scaling factor by which the vertex positions should be multiplied after extracting them from the heightmap. 

If the indices of a pixel are (```x```, ```y```) – integers ranging from (```xStart```, ```yStart```) to (```xEnd```, ```yEnd```) – and the value stored in the heightmap at that location is ```h``` – an integer ranging from 0 to ```USHRT_MAX``` (2<sup>16</sup> – 1) – then the position of the vertex in object space should be ```vec3(x, static_cast<float>(h) / USHRT_MAX, y) * scale```.  **Note the need to use floating point division when you divide h by USHRT_MAX!**

In openFrameworks, to get the value stored in the height map at pixel (```x```, ```y```), use:
```c++
heightmap.getColor(x, y).r // only one channel (red)
```
Note that ```ofShortPixels::getColor``` returns an ```ofShortColor```, which is a color object with 16 bits per channel.  Counterintuitively, this is actually more bits per channel than ```ofColor```, which has 8 bits per channel.  If you try to convert the color returned to a regular ```ofColor```, it will cause problems.

To build the mesh, you need to first define the vertices, then specify the order of the vertices using the index buffer.  You’ll need to establish a logical order for the vertices to be stored in the mesh and then add indices so that each set of three indices corresponds to the vertices of a triangle in the mesh.

If you're not sure where to start, one option is to look at the sphere mesh example that I did in in one of the required videos.  Another is the official documentation for the ```ofMesh``` class, which actually includes an example with a grid that's similar to what you have to do on the assignment: https://openframeworks.cc/documentation/3d/ofMesh/

### Using buildTerrainMesh
Once you’ve written the ```buildTerrainMesh``` function, you need to call it to actually create the terrain.  The first step is to load the heightmap.  I recommend using the low-resolution one to start with, which is included in the main download for the project.  To load the image, add the following code to ```ofApp::setup():```'

```c++
ofShortImage heightmap {};
heightmap.setUseTexture(false);
heightmap.load("TamrielLowRes.png"); // or "RandomLowRes.png"
assert(heightmapImg.getWidth() != 0 && heightmapImg.getHeight() != 0);
```

We set ```useTexture``` to false for our heightmap so that it doesn’t load an OpenGL texture – we’re never going to actually use this image in a shader, so that would be a waste of graphics resources.

Assuming you have an ```ofMesh``` called ```terrainMesh``` declared in ofApp.h, you can then use ```buildTerrainMesh()``` as follows:

```c++
// Build terrain mesh.
buildTerrainMesh(terrainMesh, heightmap, 0, 0, heightmap.getWidth() - 1, heightmap.getHeight() - 1, vec3(1, 50, 1));
```

Feel free to play around with the y-axis scale a bit if you want to.  The random noise heightmap will probably look better with a smaller y-axis scale (around 25 instead of 50).

### Rendering the terrain

To see if your terrain mesh is correct, you’ll need a shader to render the heightmap in 3D perspective.  This can be similar to your shader from the previous assignment.  I also recommend carrying over your camera controls from the previous project so that it’s easy to look around in case the terrain isn’t where you expected it to be relative to the camera.
For the fragment shader, set the color to something hardcoded initially to make it obvious if triangles are actually appearing on screen.  Use a distinctive color, like red (```vec4(1,0,0,1)```).  If you’ve tried using ```flatNormals()```, comment it out until you know that you have your vertex and index buffers and shaders set up correctly.
Once you have something visible on screen, use ```flatNormals()``` to assign normal vectors to the mesh so that it’s easy to see where are triangles are and in which direction they face.  Don’t forget to flip the normal afterwards by multiplying each normal vector by -1.  Use a shader like the one from the previous project to make sure that the normals look reasonable.

## Part 2 (50 points): “Immersive” open world experience
For the second part of the project, you will use the terrain mesh you constructed in part 1 to create an “immersive” open world experience.  For full credit, you should add all of the following features:
* (15 points) Modify your fragment shader so that the terrain is lit by directional and ambient light sources with Lambertian (constant) diffuse reflectance, as shown in the course videos on lighting / shading.  Your shading calculations should be properly gamma corrected.  In addition, set an appropriate background color for the sky using ```ofSetBackgroundColor()```.
* (25 points) Render the terrain at a higher level of detail when the camera gets close.  A class called ```CellManager``` has been provided to dynamically load and unload high resolution “chunks” of terrain so they don’t all need to be in memory at the same time.  In order for this to work, you will need to adapt the scale of your world to match the high resolution heightmap.
* (10 points) Add a water plane in a color that’s distinguishable from your terrain so that you can see where there’s water and where there’s land.  After adjusting the y-axis scale to account for the larger heightmap I suggest using a y-coordinate of about 700.0 for the water in the Tamriel heightmap, or a y-coordinate of about 400.0 for the water in the random noise heightmap. Make sure that the plane is big enough to cover all the visible terrain.  

Rendering the terrain at a higher level of detail is the most complex of these requirements, so some hints / tips to get you started are provided below.  

### Using CellManager
First, load a high-resolution height map as another ```ofShortImage```.  Make sure it’s a member variable of ```ofApp``` so that it can continue to be referenced later.  High-resolution versions of the Tamriel and random noise heightmaps are available as separate downloads on Canvas (not include in the main download due to their size).

Next, you will need to use the provided ```CellManager``` class to dynamically construct load “chunks” of a high-resolution version of the world that will be rendered at a closer distance.  You will need to ```#include "CellManager.h"``` in ofApp.h.

Then add an instance of ```CellManager``` to ofApp.h:
```c++
CellManager<5> cellManager { heightmapHiRes, 1600, 256 };
```

The type parameter of ```CellManager``` (5 in this example) determines how many cells away you can see.  The constructor parameters for ```CellManager``` provide a reference to the heightmap, the y-axis scale, and the size of each cell.  If you’re getting poor framerates or crashes on your machine, or you need to adjust for a heightmap that you found rather than using the Tamriel heightmap, an appendix provides more documentation for ```CellManager```.

Since the high-resolution heightmap is about 32 times bigger than the low-resolution version, and we use a convention of 1 pixel = 1 world space unit, the y-axis scale should increase by a factor of 32 as well.  (As before, if you’re using the random noise heightmap you may want a smaller y-axis scale: try 800 rather than 1600).  

With this increase in scale, you will also need to scale up the x- and z-axes of the low-resolution heightmap to match the high-resolution terrain and the new y-axis scale determined by high-res terrain.  This can be done by scaling the x- and z-components of the last parameter to ```buildTerrainMesh()```.  It may be helpful to calculate the scaling factor explicitly (it should come out to about 32 for either heightmap pair):
```c++
float scale { (heightmapHiRes.getHeight() - 1) / (heightmapLoRes.getHeight() – 1) };
```

With a bigger world, you’ll now need to define a useful starting position for the camera / character in the world.  I recommend starting somewhere in the middle of the world, perhaps just above the water: 
```c++
// Position character in the middle of the world.
characterPosition = vec3(
    (heightmapHiRes.getWidth() - 1) * 0.5f, 
    720, // or 420 if using random noise heightmap
    (heightmapHiRes.getHeight() - 1) * 0.5f );
```

Then, in ```ofApp::setup()```, in addition to loading the low-resolution heightmap as before, add the following initialization code for the high resolution cell manager:
```c++
cellManager.initializeForPosition(characterPosition);
```

Next, in ```ofApp::update()```, add:

```c++
cellManager.optimizeForPosition(characterPosition);
```

This function will tell the cell manager to destroy any terrain “chunks” that are too far away and call ```buildTerrainMesh()``` to create new “chunks” that are closer to the camera.  The actual execution of buildTerrainMesh will happen in a separate thread to avoid stuttering.

Then, in ```ofApp::draw()```, use the following command to draw the terrain meshes (with an appropriate shader active):
```c++
cellManager.drawActiveCells(characterPosition, farPlane);
```
This draws all the cells that are no further away than the specified farPlane.

Finally, in ```ofApp::exit()``` (a method that hasn't been present in past projects but which has been set up for you specifically in the starter code for Project 3), call:
```c++
cellManager.stop();
```
This will stop the thread that calls ```buildTerrainMesh()```.

As mentioned earlier, an appendix provides additional documentation for ```CellManager```, which you are encouraged to refer to if anything here doesn’t make sense.


With the new scale of your world, you will need to adjust the clipping planes for your projection matrices appropriately.  You should have a different projection matrix for the distant, low-LOD terrain than you do for the near, high-LOD terrain.  In order for this to work, you’ll need to draw the low-LOD / distant terrain first, then clear the depth buffer (as discussed in one of the lectures):
```c++
glClear(GL_DEPTH_BUFFER_BIT);
```
After clearing the depth buffer, you can draw the high-LOD / close terrain.

### Other requirements
If you don’t modify your fragment shader, you will probably have noticeable seams at the transition from the high level-of-detail and the lower level-of-detail.  To avoid this, enable alpha blending and have your fragment shaders set an alpha component of the output color that fades to 0.0 as the distance from the camera approaches the far clipping plane value.  This will be very similar to how you should have implemented fog for Project 2.  **A reasonably seamless transition is a requirement for full credit.**

You will need to draw the water twice for it to cover both the distant land and the nearby land.  In total, your project should have distant land, distant water, near land and near water.  Don’t forget to draw the distant land and water first and clear the depth buffer before drawing nearby land and water.

### Just for fun
If you want to make your character be affected by gravity to make the project feel more like an open world game, a class called ```CharacterPhysics``` has been provided to help with this (documented in an appendix).  No extra credit will be awarded for this (and you won’t lose points for not having it), but it should be fairly easy to add once everything else works.

If you try this, you may find it helpful to enable “depth clamping” while rendering your water, since your character may get very close to the water’s surface, making it otherwise difficult to set a good near plane: ```glEnable(GL_DEPTH_CLAMP);```

## Submission
Make sure that all of the following are committed to your Git repository in the appropriate directory:
* main.cpp, ofApp.h, and ofApp.cpp
* buildTerrainMesh.cpp
* Your vertex and fragment shaders
* Any other .h or .cpp files that you created.
* If you found a heightmap other than the Tamriel heightmap, you should add that too.

**To indicate that all files have been committed and pushed and that your project is ready for grading, submit the URL of the repo to Canvas for the full "Project 3" assignment. (even if it's the same URL as for past projects and/or the check-in for this project).**

## Appendix A: CellManager details
```CellManager``` is a class template that takes an integer template parameter (template parameters don’t have to be types; an integer template parameter is not uncommon).  This parameter controls the number of terrain meshes that will be loaded at a given time.  Each terrain mesh will only contain a part of the world, which will make it more practical to render.  Each partial terrain mesh is called a “cell.”  A ```CellManager<P>``` will have 2```P``` x 2```P``` grid of cells (4```P```<sup>2</sup> cells total).

The ```CellManager``` constructor takes three parameters:
```c++
CellManager(const ofShortImage& heightmap, float heightmapScale, unsigned int cellSize)
```
```heightmap``` is a reference to an ```ofShortImage``` containing the heightmap (which it will use to construct terrain meshes on the fly, using the ```buildTerrainMesh()``` function you wrote earlier).  

```heightmapScale``` is the *y*-axis component of the scale that will be passed to ```buildTerrainMesh```.

```cellSize``` determines how much terrain is loaded into a single cell.  The amount of land that gets drawn at a given time is determine both by the number of cells (controlled by ```CellManager```'s template parameter ```P```) and the size of each cell.

If you are having trouble getting the world to load or render at a reasonable frame rate, you could reduce either the type parameter or the cell size.

```CellManager``` has four functions that you’ll need to call at various points in your program.

```c++
void initializeForPosition(glm::vec3 position);
```
This function should be called in your ```ofApp::setup()``` function.  Pass in whatever position you want the loaded terrain to be centered around.

```c++
void optimizeForPosition(glm::vec3 position);
```
This function would be called in your ```ofApp::update()``` function to unload cells that have gotten to be far away and request new cells that have gotten closer.

```c++ 
void stop()
```
This function would be called in ```ofApp::exit()``` (not part of the default openFrameworks template but has been explicitly added to the starter code for Project 3).  This stops the thread that loads cells in the background in order to prevent the program from crashing on exit.

```c++
void drawActiveCells(glm::vec3 camPosition, float drawDistance)
```
This function iterates over all the available cells and draws all of them that are within the draw distance from the current camera position.  This should be called from your ```ofApp::draw()``` function.  The draw distance is the same as the far plane from your projection matrix.

## Appendix B: Supporting code for “just for fun” stretch goals
### The ```World``` class
In order to use the ```CharacterPhysics``` class for player movement, you will need to define and configure an instance of the ```World``` class.  This class is mostly just a holder for core pieces of information about the world: the heightmap, the dimensions to which the world will be scaled, the rate of gravity, and the water height.  I recommend setting up a ```World``` object for the Tamriel heightmap as follows:
```c++
// Setup the world parameters.
float heightmapScale { 800 };
world.heightmap = &heightmap.getPixels();
world.dimensions = vec3((heightmap.getWidth() - 1), heightmapScale, heightmap.getHeight() - 1);
world.gravity = -world.dimensions.y * 0.05f;
world.waterHeight = 700; // or 400 for random noise
```
You can adjust the water height and gravity as you see best; these parameters worked well for me with the high-resolution Tamriel heightmap.  Once you have a ```World``` instance set up, you can call the following function to get the height of the terrain under / above any position in 3D space:
```c++
float getTerrainHeightAtPosition(const glm::vec3& position) const;
```
If you use CharacterPhysics, you probably won’t need to call this function manually.

### The ```CharacterPhysics``` class
```CharacterPhysics``` has a constructor that takes reference to a World instance as a parameter:
```c++
CharacterPhysics(const World& world);
```
To initialize the character, you’ll want to set its position and the “character height.”  Getters and setters are provided for both:
```c++
glm::vec3 getPosition() const;
void setPosition(const glm::vec3& position);
float getCharacterHeight() const;
void setCharacterHeight(float characterHeight);
```
For the physics to feel right, I recommend taking the absolute value of the gravity parameter you set up for your ```World``` instance and multiplying it by 0.1685 to get the character height.  (This assumes that gravity in the real world is 9.8 meters / sec<sup>2</sup>, and the average human is about 1.65 meters tall; thus the ratio between the character height and gravity is 1.65 / 9.8 = 0.1685.)

The “position” of the character is really the position of the character’s head (taking both the height of the terrain and the character’s height into account).  You’ll want to use this position as your camera position when you set up your viewing transformation.

```CharacterPhysics``` has three more functions that you will want to use:
```c++
void setDesiredVelocity(glm::vec3 velocity) const;
```
This sets the character’s intended velocity in world space, which will come from your input device (mouse & keyboard).  This velocity will be constrained by an approximation of the laws of physics – in particular, that a character on the ground will stay on the ground unless they run off a cliff, and a character falling in the air cannot change their velocity until they are back on the ground.

Your input device will most likely determine the character’s velocity in its own local coordinate space.   You will need transform it to world space by inverting the head rotation.  The world space velocity can then be passed to ```setDesiredVelocity```.

As with the character’s height, it may be useful to use the world’s gravity to calibrate the scale of the velocity – for example, establish that the magnitude of the velocity is always 0.5 times ```abs(gravity)```.

```c++
void jump(float speed);
```
If the character is on the ground, this function increases the y-velocity so that they jump into the air.  If the character is not on the ground, this function does nothing (no double-jumping).

```c++
void update(float dt);
```
This function advances time by ```dt``` for the character, updating the character velocity and position appropriately.  This should be called from ```ofApp::update()```, using ```ofGetLastFrameTime()``` as ```dt```.
