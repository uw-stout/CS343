#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    // IMPORTANT!  Don't remove this line or textures won't work:
    ofDisableArbTex();
}

void ofApp::reloadShaders()
{
    // Load shaders here:

}

//--------------------------------------------------------------
void ofApp::update()
{
    // First, check for shader reload via hotkey:
    if (needsReload)
    {
        reloadShaders();
        needsReload = false;
    }

    // Add additional update logic as needed:

}

//--------------------------------------------------------------
void ofApp::draw()
{

}

//--------------------------------------------------------------
void ofApp::exit()
{

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    // Shader reload hotkey
    if (key == '`')
    {
        needsReload = true;
    }

    // Handle other key press events as needed:

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y )
{

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y)
{

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y)
{

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg)
{

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{ 

}
