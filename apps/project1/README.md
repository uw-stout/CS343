# Project 1: Particle Effects (100 points)

Particle effects are commonly used in computer graphics to convey an impression of phenomena like fire, smoke, lightning, magic, and more.  In this assignment, you will implement a simple particle effect and render it in real time to a window using openFrameworks.
Here are a few reminders as you get started with openFrameworks:
* C++ source files need to go in the “src” folder for an openFrameworks project.  This is not the Visual Studio default.  If you create your C++ files from within Solution Explorer, make sure you specify “<your solution name>/src” as the directory.
* Similarly, shader files need to go in “bin/data.”
* To get GLSL syntax highlighting, download the following Visual Studio extension: https://marketplace.visualstudio.com/items?itemName=DanielScherzer.GLSL
* GLSL syntax errors will be displayed in the “Output” panel at runtime (not in the “Error List” or in the console).
* Make sure that your textures load successfully:
```assert(img.getWidth() != 0 && img.getHeight() != 0);```

To help you get started with this project, a few resources have been provided:
* There is a very nice collection of Creative-Commons-licensed particle effect textures available here: https://www.kenney.nl/assets/particle-pack  For convenience, I’ve included them as part of the repository for this project.
* I’ve written a ```ParticleSystem``` class template that provides a lot of the boilerplate that you need for this assignment.  This class template requires that you implement the ```Particle``` and ```ParticleGenerator``` interfaces defined in Particle.h.

## Early check-in (20 points)
For this project, you are required to complete an early check-in.  The early check-in is worth 20 points and will be evaluated as follows:
* 10 points for checking in regardless of where you’re at (even if it doesn’t work at all).
* 10 points for making substantial, visible progress on Part 1.

I can check you off either during class time or in office hours.  **You should also add me (@tetzlaff) as a member with "Reporter"-level (read-only) access to the repository and submit the URL to Canvas for the "Project 1 check-in" assignment.**

## Part 1 (40 points): Animation using a particle system
To begin, open project1.sln and add code to render one of the textures in the “particle pack” to the screen.  This should be very similar to “Project 0.”  Write a shader that alters the texture’s color (which would otherwise be grayscale) so that it looks like a specific effect that you want to convey (like fire, lightning, etc.).

To complete the first part of this project, create a particle system that renders multiple particles to the screen simultaneously.  It is recommended that you use the ```ParticleSystem``` class template that has been provided for you.  Choose an appropriate blending mode (alpha or additive) for the effect you are trying to convey.  

To use the ```ParticleSystem``` class template, you will need to first create classes that implement the ```Particle``` and ParticleGenerator interfaces defined in Particle.h. 
* Your ```Particle``` implementation needs to have at least two functions, a getter function for the particle’s life (```getLife()```) and an ```update()``` function that updates the particle’s position, life, and any other information that changes over time.  To get access to openFrameworks and GLM classes and functions (like ```vec2``` / ```vec3```), you can add #include "ofMain.h" to the top of your implementation file.
* The ParticleGenerator implementation just needs to define a function called ```respawn()``` that takes a reference to a particle as a parameter and modifies that particle as if it were a newly spawned particle.  This will generally mean resetting its life, and randomizing position and any other information that should vary between particles.

To generate random numbers, use the ```rand()``` function:
```c++
int r = rand();
```
This  function returns an int in the range 0 to ```RAND_MAX``` (a very large integer).  To obtain an integer in a smaller range, you can use the modulus operator (%).  To obtain a floating point number, you can use the modulus operator first, and then divide by a floating point number corresponding to the largest possible integer that could be generated.  

For example:
```c++
int r = rand() % 1001; // Generates a random number between 0 and 1000.
double x = r / 1000.0; // Results in a random floating-point number between 
                       // 0.0 and 1.0, with precision to the 100ths place.
```

Once you’ve written classes that implement ```Particle``` and ParticleGenerator, you can use the ```ParticleSystem``` to do the heavy lifting.  The intended usage is as follows:
* In ofApp.h, declare and initialize your particle generator and particle system:
```c++
  MyParticleGenerator particleGenerator { /* any parameters go here */ };
  ParticleSystem<MyParticle> particleSystem { particleGenerator, 42, 20.0f };
```
* The last two parameters of the ```ParticleSystem``` constructor define the maximum number of particles and the rate at which particles spawn.  The maximum number of particles needs to be set so that particles don’t need to be killed prematurely.  Practically speaking, this means that the second parameter (number of particles) needs to be greater than or equal to the third parameter (particle rate) times the typical lifespan of a particle.  For instance, if a particle typically lasts for two seconds before its life reaches zero, then the number of particles needs to be more than twice the particle rate.  Tweak these parameters until your particle system looks the way that you want it to look.
* In ```ofApp::update()```, call: ```particleSystem.update(ofGetLastFrameTime());```
* ```ParticleSystem``` has ```begin()``` and ```end()``` functions defined (not to be confused with the unrelated ```shader.begin()``` and ```shader.end()``` semantics), which provide access to iterators over the particles which in turn allows for the use of a range-based for loop over the particles.  Move your drawing code that needs to be called for each particle (setting particle-specific uniforms and then actually drawing the quad mesh) inside of a range-based for loop over the particle system. 

## Part 2 (40 points): Multiple textures and multiple particle systems
Once you have a basic particle system working, you can extend it in several ways:
1.	[10 points] Make the particles appear and disappear seamlessly by programming the particle’s intensity to start at zero, fade in quickly (but not instantly) at the beginning of the particle’s life, and then fade out gradually.
2.	[10 points] Give your particles a velocity assigned randomly when they are initialized.  If you want, you can also give them some acceleration or deceleration so that the velocity changes over time.
3.	[10 points] Load two (or more) different textures that a particle can use and randomly decide which one each particle will use when it is spawned.  This will make your particle effect look less repetitive.
4.	[10 points] Add a second complementary particle system.  For example, if your first particle system was fire, you could do smoke for the second particle system.  You can probably reuse a lot of the code from the first system.  Your second particle system should differ from the first in two ways:
    * The second particle system should use different textures.
    *	The second particle system should use a different blending mode.  So if you did additive blending for the first particle system, do alpha blending for the second, or vice-versa.  Remember to use additive blending for emissive effects (light, fire, lightining, etc.) and alpha blending for occluding effects (smoke, fog, clouds, etc.).  **To receive full credit for both particle systems, the choice of blending mode must be appropriate for each one.**

Both requirements for the second particle system must be met for full credit.  Partial credit will be given for completing one of these two requirements.  For full credit, the particles of the second system should also fade in and out seamlessly (see #1 above).  You do not necessarily need to complete extensions #2 and #3 for both particle systems. 

## Submission
Make sure that all of the following are committed to your Git repository in the appropriate directory:
* main.cpp, ofApp.h, and ofApp.cpp
* Your classes that implement the ```Particle``` and ```ParticleGenerator``` interfaces 
* Any other .h or .cpp files that you created.
* Your vertex and fragment shaders

**To indicate that all files have been committed and pushed and that your project is ready for grading, submit the URL of the repo to Canvas for the full "Project 1" assignment. (even if it's the same URL as for Project 0 and the check-in for this project).**
