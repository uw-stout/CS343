# Final Project (300 points)

For your final project, you get to explore an area of computer graphics that you find interesting, and which we haven’t covered yet in this course.  **You may work independently, or with one or two others on this project.**  Each group will be doing their own project, so you will need to submit a proposal to make sure your project is aiming for the right requirements.  (My goal is to give you timely feedback on these proposals so that if you need to make adjustments, you can.)  If you choose to work in a **group of three**, your proposal **must include a description of intended roles**, and part of your grade will be an individual assessment of how well your role on the project was completed.  Besides giving you an opportunity to achieve some of your own goals related to graphics, this project is intended to expand your view of graphics beyond the basic rendering pipeline that we’ve been using all semester.  You should also do an optimization experiment where you study how implementation details affect performance.  You will present the results of this experiment to the class during our “final exam” period, along with a demonstration of your project.  Both partners must participate in the presentation.

## Proposal (20 points)
Your proposal should describe, in concrete terms, what you will do to fulfill each of the four key requirements of the project (see below).  Your proposal must include:
* One or two sources that you’ll be using as a reference.  These can be from either of the course textbooks, or resources that you found online.
* A description of the programming work you will need to do after working through tutorials.
* At least one idea for how you will fulfill the optimization aspect of the project.  
* A short, high-level description of the code structure for your project (one or two sentences or a simple UML diagram should be sufficient). 

Email your proposal to your instructor (tetzlaffm@uwstout.edu) with any/all partners CC'ed.

## Early check-in (20 points)
The early check-in is worth 20 points and will be evaluated as follows:
* 10 points for anything showing me where you’re at (even if it doesn’t work at all).
* 10 points for making substantial, visible progress on the final project.

I can check you off either during class time or in office hours.  **You should also add me (@tetzlaff) as a member with "Reporter"-level (read-only) access to the repository and submit the URL to Canvas for the "Final project check-in" assignment.**

## Project requirements and rubric (200 points)
There are two components that your project must have and on which it will be evaluated:

**Advanced graphics programming (100 points)** – For this requirement, you have many choices:
* Your project can include an effect produced by rendering to an offscreen framebuffer.  If you do your final project using openFrameworks, this would mean using an ofFbo.  This offscreen framebuffer would then be used as a texture in the primary rendering pass.  There are some suggestions for effects that you can achieve using this technique later in the document.
* You can implement your final project by doing shader programming in a game engine like Unity, Unreal Engine, or Godot.  (These engines all do offscreen rendering as part of their core graphics pipeline, so if you use one of these engines, you are also, in a way, doing offscreen rendering.)
* You could explore the use of acceleration data structures like an octree or a bounding volume hierarchy.  Make sure that you define a problem that will actually benefit from this.
* You could explore an aspect of procedural modelling.  Ideas for this include L-systems (for rendering fractals or natural elements like trees or plants) or Bezier curves / splines.
* You could explore methods of visualizing a 3D texture (volume rendering) or higher dimensional functions (isosurfaces).
* You could write a 3D web application using WebGL.  WebGL is more low-level than openFrameworks (and thus more difficult to work with), but is not quite as low-level as native OpenGL.  This wouldn’t need to be a particularly complicated application; it could just be a port of one of the earlier assignments from this course.
* You can write a graphics program that does not use the GPU.  You might write your own rasterizer and clipper to better understand how that part of the pipeline is implemented, or you could implement a non-raster graphics system like a ray tracer.

You should organize your code in a thoughtful way.  This requirement can be fulfilled either by using object-oriented principles in C++ with openFrameworks, or by correctly using the architecture of a game engine like Unity, Unreal Engine, or Godot.  If you do use openFrameworks, your code structure should not just be “put everything in ofApp.cpp.”  

**You must do more than simply work through a chapter of the Halladay text or an online tutorial to receive full credit.**  You should need to do some additional programming after finishing any tutorials.  If you’re not sure if you’ve done enough of your own programming to get full credit, ask the instructor.  Some of the extra programming work may also fulfill the optimization requirement.  **To receive full credit, you must include a writeup or README** (you can archive / replace / add to this README) ** that credits sources for all of your code and explains what your extension was above and beyond those sources.**

**Optimization (100 points)** – You should try implementing some aspect of the project in ***two*** or more ways and decide which one to use based either on which is more efficient (resulting in smoother framerates), or which produces higher quality (better looking) results.  You may observe a trade-off between the two.  You may also find that there is no noticeable difference, ***but*** you should make an effort to follow ***all of the optimization strategies discussed in the textbook and lecture videos*** before concluding that your project can’t be optimized further.  **You must quantify your results in terms of measures like frame time / framerate**, and compare at least ***two*** implementations that you considered to receive full credit.  **You should describe at least one idea you have for this aspect of the project in your proposal.**  

Optimizations that you could consider include:
* Placement of calculations: in the C++ code (or C# / Blueprints if you’re using a game engine), or the vertex shader, or the fragment shader (note that these may be effectively combined or somewhat ambiguous in a game engine).
* Order of operations: i.e. multiply then add, or add then multiply.  (Make sure that the alternatives you consider are still mathematically equivalent.)
* The use or non-use of branching (i.e. if-statements) in shader code.
* Using a built-in shader function (like mix, clamp, dot, reflect, etc.) or not.
* The order that objects are rendered to the screen.
* You could also study two alternatives that are NOT mathematically equivalent for a particular effect, where you expect to see a trade-off between rendering quality and performance.

You could also develop an openFrameworks app for a mobile device (iOS and Android are both supported: https://openframeworks.cc/download/) and experiment with different levels of floating-point precision.  If you go this route, simply reproducing one of the textbook examples, successfully deploying it on a mobile device, and investigating the impact of precision qualifiers would be sufficient to fulfill the requirements of the project.  This optimization experiment could also apply if you decide to explore the use of WebGL.

You should discuss the results of your optimization experiment in your presentation.  Your submitted code should contain both options, with the ability to switch between the two by making a small change in the code. 

If you do your final project in a game engine like Unity, Unreal Engine, or Godot, these engines tend to be optimized fairly well, so you may just want to investigate two different approaches for producing a certain effect and choose the one that looks the best.  If, as you’re working on the project, you come across an optimization issue that you didn’t anticipate, you can also use that to fulfill this requirement instead of what you wrote in your original proposal.

**It may be the case that you have a good idea for the final project, but the requirements don’t quite fit nicely into these categories.**  If that is the case, feel free to propose it anyways; if necessary, we can negotiate an alternative rubric for how your project will be evaluated.  If you’re unsure of how to construct such a proposal, feel free to send me an email or discuss it during class or office hours.

## Presentation (60 points)
You and your partner will present your results during our class’s final exam period.  **Attendance for this presentation session is required.**

Your presentation should be a ***maximum*** of **five minutes** long (and should not be much less than that).  Your presentation should briefly describe what you did, show some results (pre-recorded video or screenshots; there won’t be time for you to load a live demo), and discuss the results of your optimization experiment.  **You will be graded individually** based on the clarity of the content that you contribute to the presentation.

## Final project ideas
Here are some more detailed descriptions of ideas that you could implement for your final project.  You do not need to choose something from this list if you have another idea.
* **Implement an advanced lighting effect in openFrameworks that requires the use of multiple drawing passes.**  You could start with the heightmap project that you’ve used for Assignments 2-4, or one of the textbook examples as the starting point for your scene.  Possibilities include:
    * Real-time shadows
    * Real-time reflections
    * Subsurface scattering for human skin
    * Screen space effects like ambient occlusion, depth-of-field, motion blur, etc.
    * Tone-mapping
* **Implement an app that renders a 3D texture** in openFrameworks or another tool of your choice.  In openFrameworks, you will need to manually add support for a 3D texture.  You can use the cubemap code as a starting point for this.  You should be able to find publicly available 3D texture data, often from the medical industry (i.e. CT / MRI scans).
* **Implement an app in openFrameworks that renders an L-system** (https://en.wikipedia.org/wiki/L-system).
* **Implement an app in openFrameworks that renders a Bezier curve** (https://en.wikipedia.org/wiki/B%C3%A9zier_curve), or **cubic Hermite spline** (https://en.wikipedia.org/wiki/Cubic_Hermite_spline), or **animate an object so that it follows the path of a Bezier curve or spline.**  
* **Implement a custom shader in Unity, Unreal Engine, or Godot.**  Since these engines are pretty good at physically based rendering with the default shaders, you may want to try something non-photorealistic.
* **Create and deploy an openFrameworks project on a mobile device** (iOS or Android), and investigate the impact of shader variable precision (as discussed above).
* **Port one of the assignments from this course to WebGL**, and investigate the impact of shader variable precision (as discussed above).
* **Implement your own graphics system without using a graphics API**.  You have two main choices for this: a raster graphics system (essentially a simpler version of what OpenGL / openFrameworks do) or a ray tracer.  I would recommend that you still use openFrameworks for getting something to display on the screen, but instead of setting up shaders like you would normally, manually set the pixel colors in an ofImage object, and then simply render that ofImage onto the screen.  ofImage has a draw() function that you should be able to use to put it onto the screen without even using a shader or mesh.
    * For **raster graphics**, you would need to research Bresenham’s line algorithm (https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm) and the Cohen-Sutherland clipping algorithm (https://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm).  Drawing the outlines of a couple of triangles using these algorithms would be sufficient to fulfill the requirements of the project.  You will still need to fulfill the structure and optimization aspects of the project.
    * For **ray tracing**, simply modelling direct illumination plus one ray tracing effect would be sufficient.  Your scene should be very simple (stick with simple primitives like spheres or triangles or planes).  
        * Ray tracing effects that you could consider include:
            * Ray-traced shadows
            * Ray-traced specular reflections
            * Ray-traced refraction through a semi-transparent surface
        * Here are a few tutorials that you can reference if you want to attempt a ray tracer:
            * https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-ray-tracing
            * https://raytracing.github.io/books/RayTracingInOneWeekend.html
        * If you use any code from those tutorials, make sure you cite your source.  You don’t need to implement all the features described in either of those tutorials.  You should, however, make sure that you address the structure and optimization aspects of the final project requirements.

## Submission
Commit **all of the source files** for your project to your Git repository, along with a **writeup or README** crediting all your sources and clarifying **what your own additions to the project were.**  To be safe, upload everything unless you are certain that it isn’t needed.  (One way to check is to clone another copy of the repo yourself and try building and running it.)  

The format of the files may vary depending on whether you chose to use openFrameworks, Unity, Unreal, Godot, or something else for your final project.  A starter project has been created in case you're using openFrameworks.  If you don't use openFrameworks, you can just delete all of those files and replace them with your own project files in whatever environment you're using.  If necessary, you can also create an empty repository for your project if that makes more sense, or if you are working with new or different teammates than earlier in the semester.  

**To indicate that all files have been committed and pushed and that your project is ready for grading, submit the URL of the repo to Canvas for the full "Final Project" assignment. (even if it's the same URL as for past projects and/or the check-in for the final project).**
