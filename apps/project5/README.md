# Project 5: Environment illumination and specularity (120 points)
This project consists of two parts.  The **first part** focuses on texture mapping.  The second part focuses on specularity and reflections.  Each of these parts has multiple options to give you some freedom as you complete the project.  Certain options for the second part may depend on a particular choice for the first part, so you are encouraged to read through the full instructions before starting part 1.  **However, you are not allowed to use the shield model from the lectures for this project.**

As with past projects, it is recommended, but not required, that you copy over code for an earlier project as a starting point for this project.  **If you don’t use one of these projects as a starting point, it is required that you still provide equivalent camera controls** (camera pitch / head; move forwards / backwards / left / right, and some kind of vertical motion, which could be a “flying camera” or jumping).  Ask your instructor for a solution if you aren’t satisfied with where you ended up on earlier projects.

The repository for this project contains the following supplemental files:
* src (supporting C++ code as used in the lecture videos)
    * calcTangents.h
    * calcTangents.cpp
    * ofxCubemap.h
    * ofxCubemap.cpp
* bin/data/textures
    * aerial_grass_rock_diff_4k.png (diffuse texture, for terrain)
    * aerial_grass_rock_nor_4k.png (normal map for terrain)
    * aerial_grass_rock_ao_4k.png (ambient occlusion map for terrain)
    * water_nrm.png (water normal map, same one used in the Halladay textbook)
    * skybox textures (front, back, left, right, top, bottom)
    * irradiance textures (front, back, left, right, top, bottom)

## Early check-in (20 points)
The early check-in is worth 20 points and will be evaluated as follows:
* 10 points for anything showing me where you’re at (even if it doesn’t work at all).
* 10 points for making substantial, visible progress on part 1.

I can check you off either during class time or in office hours.  **You should also add me (@tetzlaff) as a member with "Reporter"-level (read-only) access to the repository and submit the URL to Canvas for the "Project 5 check-in" assignment.**

## Part 1 (60 points): Texture mapping for 3D surfaces
To complete part 1, open project5.sln and implement a program that renders a scene meeting the following requirements:
* (10 points) The scene must have a skybox.
* (10 points) The scene must have at least one object or surface with a color texture applied to it.  You should enable mipmaps for the texture.
* (10 points) The scene must have at least one object with a normal map applied to it.  You should enable mipmaps for the normal map.  For convenience, the ```calcTangents()``` function has been provided in the repository.
* (20 points) All objects in the scene (you don’t need to have more than one) must be illuminated by at least **two** light sources (not counting ambient light or the irradiance map).  Note that this is one less than for Project 4, but more than the single light source demonstrated in lecture.
* (10 points) All diffuse objects in the scene (you don’t need to have more than one) should have an irradiance map applied to them for ambient lighting that is consistent with the skybox.  You can either use the mipmap hack shown in the videos, or you can load a separate irradiance cubemap (one is provided in the supporting files).  Either will count for full credit, but a true irradiance map is the preferred option.

All shaders should have appropriate gamma correction, both when sampling color textures and when calculating the final fragment color.

Here are several suggestions for how to complete these requirements, some of which relate to solutions from past assignments:
* You could add a diffuse texture map to your heightmap-based terrain mesh.  
    * Feel free to ask your instructor to help with implementation of texture coordinates for the terrain.  You will also want to set the texture wrap mode to ```GL_REPEAT``` and set up appropriate UV coordinates to allow the texture to tile across the terrain. 
* If you have multiple level-of-detail terrain working, the skybox should be rendered before the distant terrain.  
* You can also put your normal map on the terrain.  Ask your instructor for an implementation of smooth normal calculations for the heightmap if you don't want the terrain to look blocky due to the flat normal calculation you did for Project 3.  As an optional challenge (no extra credit), there’s an ambient occlusion map included in the supporting files that you could use to modulate the sky irradiance.
* If you don’t want to do terrain texturing, you could download a textured model that contains normal off of a website such as Sketchfab:
https://sketchfab.com/3d-models?features=downloadable&sort_by=-likeCount
Here’s an example of a model that would work well: 
https://sketchfab.com/3d-models/zweihander-sword-game-model-4a55f34fe5e645bca91c603aae222219 
* You could work off of the water shader developed in chapters 10 and 11 of the Halladay textbook, which includes a normal map (and continue to use that shader for the specularity requirements of Part 2).  
    * If you’re combining it with the heightmap-based terrain project, you will probably want to adjust the scale of the UV coordinates to account for the fact that the water plane is likely much larger in your project then it is in the textbook examples. 
    * You may need to adjust the scale of both the UVs and the “time” variable provided to the shader for animation (to calibrate both the size and speed of the waves).
* It is not recommended to texture the robot from Project 4 as the primitive shapes don’t have well-defined texture coordinates.  If you want to use Project 4 as a base for this project, add a simple ground plane to fulfill all the texturing requirements of the assignment.  Your robot should still meet all the lighting requirements and you may choose to make it metallic or shiny.
* You can also find your own textured 3D model, or create a textured model in Blender, but you may not use the shield model used in the lecture videos.

## Part 2 (40 points): Specular reflections & highlights
To complete part 2, extend your scene from part 1 to meet the following requirements:
* (20 points) The scene must have at least one object or surface that exhibits **specular reflections of the skybox** and **highlights from all the light sources** defined in part 1.  You should continue to apply appropriate gamma correction, both when sampling color textures and when calculating the final fragment color.  Do not apply gamma correction to textures that do not represent color.
* (10 points) The reflections should exhibit the Fresnel effect using **Schlick’s approximation.**  
* (10 points) For the highlights, you should implement the **Cook-Torrance model** shown in the video on physically-based specularity, using the **GGX model** for the microfacet distribution, the **Smith height-correlated function** for microfacet masking and shadowing, and **Schlick’s approximation** for the Fresnel effect. 

Here are several suggestions for how to complete these requirements, some of which relate to solutions from past assignments:
* You could write a metal shader for the robot from Project 3 that includes both sky reflections and highlights from your light sources.  Think about the “metallicity” approach to specular modelling, and remember that for a metal object, there should be no diffuse shading; instead the specular color should represent the overall color of the object.
* You could modify the water shader in chapters 10 and 11 of the Halladay textbook by adding the Fresnel effect to the sky reflections and using the Cook-Torrance model for the specular highlights.
    * For the Fresnel effect, use F0 = 0.02, the physically correct value for water.
    * The water will look better if you enable mipmaps for your cubemap.
* If you used a model from Sketchfab for Part 1 that comes with a specular map, you could use the provided specular map to add sky reflections and highlights from your light sources.  
Note that many models will use a metallicity map, which is NOT the same as the specular color map from the lecture videos!  If this is the case, you will need to use the metallicity map to determine an appropriate specular color at each pixel.  Remember how metallicity works:
   * If the pixel in the metallicity map is 1.0 (“white”), the pixel is metallic: the base color from the color texture is the specular color, and the diffuse color is black. 
    * If the pixel in the metallicity map is 0.0 (“black”), the pixel is non-metallic: the base color from the color texture is the diffuse color, and the specular color is vec3(0.04).
    * In practice, you will use mix() to linearly interpolate over the metallicity value.
* You can also use another 3D model either from somewhere like Sketchfab, or that created yourself in Blender, and add specularity to it.

For convenience, several formulas from the lecture videos are repeated below:

Cook-Torrance highlights:
$$
π * f = \frac{π * D * G * F }{4 (\mathbf{n}\cdot\mathbf{l}) (\mathbf{n}\cdot\mathbf{v})} = π * D * F * \frac{G}{4 (\mathbf{n}\cdot\mathbf{l}) (\mathbf{n}\cdot\mathbf{v})}
$$

A factor of $π$ is included on both sides of the equation to emphasize the common convention in game engines of omitting $π$ from reflectance equations (effectively absorbing it into light intensities).

The equations for $D$, $G$, and $F$ are:

$$
π D(h) = \frac{m^2}{\mathrm{mix}(1, m^2,(\mathbf{n}\cdot\mathbf{h})^2 )^2}
$$

$$
\frac{G(\mathbf{l},\mathbf{v})}{4 (\mathbf{n}\cdot\mathbf{l}) (\mathbf{n}\cdot\mathbf{v})} = \frac{0.5}{(\mathbf{n}\cdot\mathbf{v}) \sqrt{\mathrm{mix}((\mathbf{n}\cdot\mathbf{l})^2, 1, m^2} )+ (\mathbf{n}\cdot\mathbf{l}) \sqrt{\mathrm{mix}((\mathbf{n}\cdot\mathbf{v})^2, 1, m^2)}}
$$
(note that the entire fraction $\frac{G(\mathbf{l},\mathbf{v})}{4 (\mathbf{n}\cdot\mathbf{l}) (\mathbf{n}\cdot\mathbf{v})}$ is on the left-hand side of the preceding equation)

$$
F(\mathbf{n},\mathbf{l}) ≈ \mathrm{mix}(F_0, 1, (1-(\mathbf{h}\cdot\mathbf{l}))^5)
$$

**IMPORTANT:** If you look closely at the rendering equation, you’ll notice that $f$ is typically multiplied by the light intensity times $(\mathbf{n}\cdot\mathbf{l})$.  This means that in practice, you need to calculate $f$, then multiply by both the light intensity and $(\mathbf{n}\cdot\mathbf{l})$ to get the final specular highlight color.  This should be done for each point, directional, or spot light in your scene, and the results should be added together (the summation in the rendering equation).

## Submission
Make sure that all of the following are committed to your Git repository in the appropriate directory:

* main.cpp, ofApp.h, and ofApp.cpp
* Any other .h or .cpp files that you created or modified.
* All of your vertex and fragment shaders
* Any data files that you downloaded from Sketchfab or any other website (or a README with a link if you prefer).
* Any other data files that you created.

**To indicate that all files have been committed and pushed and that your project is ready for grading, submit the URL of the repo to Canvas for the full "Project 5" assignment. (even if it's the same URL as for past projects and/or the check-in for this project).**
