# Project 4: Scene graphs and lighting (120 points)
In this project, you will get practice with scene graphs by using them to construct an animated robot in openFrameworks.  You will also write shaders that contain point or spot light sources. 

As with the previous project, it is recommended, but not required, that you copy over code from an earlier project (Project 2 or 3) as a starting point for this project.  If you don’t use one of these projects as a starting point, it is required that you still provide equivalent camera controls (camera pitch / head; move forwards / backwards / left / right, and some kind of vertical motion, which could be a “flying camera” or jumping).  Ask your instructor for a solution if you aren’t satisfied with where you ended up on earlier projects.

The starter project for this project also comes with C++ code related to scene graphs from lecture, as well as a few primitive models (in bin/data/models) to help you build the robot:
* cone.ply
* cube.ply
* cylinder.ply
* sphere.ply
* torus.ply
 
## Early check-in (20 points)
The early check-in is worth 20 points and will be evaluated as follows:
* 10 points for anything showing me where you’re at (even if it doesn’t work at all).
* 10 points for making substantial, visible progress on Part 1.

I can check you off either during class time or in office hours.  **You should also add me (@tetzlaff) as a member with "Reporter"-level (read-only) access to the repository and submit the URL to Canvas for the "Project 4 check-in" assignment.**
 
## Part 1 (20 points): Build your own robot
To complete the first part of this project, open project4.sln and use the ```SceneGraphNode``` class that we developed in lecture to set up a simple robot.  This robot must meet the following minimum requirements:
* The robot must be composed of three or more primitives such as cubes, cones, cylinders, spheres or toruses.  
* You must sketch out a ***design*** for your robot on paper or in a drawing tool on the computer and indicate your intention for how the robot would be animated.  You may revise this design as you work through the project, but you are expected to submit this design with your solution and will be graded on the basis of whether your implementation matches the design.
* You must also turn in a sketch of the scene graph data structure for your robot with your solution (again, on paper or in the drawing application of your choice).  You are encouraged to use this sketch as a tool to figure out the correct hierarchy and where you need to apply each transformation.

For part 1, **you do not need to actually animate the robot.**  Animation is scored under part 3 of this project, and it is recommended that you wait to work on animation until after you have lighting (part 2) working.

Here are a few suggestions for part 1 of the assignment:
* When you start implementing the robot in your openFrameworks project, start with a single stationary primitive and make sure that it’s positioned in a reasonable place.  
* Incrementally add additional components and don’t be afraid to use trial and error until you get the scene graph working correctly.
* Don’t hesitate to ask for help at any stage of the process if you need it.

## Part 2 (40 points): Multiple light sources
For the second part of the project, you will add more lights to your scene.  All objects in the scene (you don’t need to have anything more than just your robot) must be illuminated by at least three discrete light sources:
* At least one of these light sources should be a spot light, and the edge of the spot should be visible on whatever is being illuminated (don’t set the cone so big you can’t see this).
* All point and spot light sources should exhibit physically correct light attenuation (“falloff”). 
* You should also have ambient light in your scene, but **it doesn’t count as a discrete light source.**

Your shading calculations should be properly gamma corrected.

Make sure it is obvious where the light sources are when you start the program (or indicate clearly how to find the area most illuminated by them in a comment message on your final commit). 

## Part 3 (40 points): Animating the robot
For part 3 of the project, you will add animation to your scene graph.  For full credit, your scene graph must meet the following requirements:
* The robot must have two or more animated components (such as rotating arms, or a rotating head, or spinning wheels).  
* At least one of the animated nodes must be a parent of another animated node.  If the robot moves around in the scene, that movement counts as an animation.
* At least one of the animated nodes must have a custom updateNode() function (i.e. not just SimpleAnimationNode)
* At least one of the light sources should be animated as a node in the scene graph.

## Just for fun: Using Project 3 as a base
You may use either Project 2 or Project 3 as a starting point for this project.  Project 2 will probably be easier.  If you do decide to work off of Project 3, the challenge is that the robot might end up hidden under the terrain, or too far away from the camera to be seen.  This can be solved by carefully positioning the robot manually, translating the robot to be near your character / camera in world space.  You can call ```world.getTerrainHeightAtPosition()``` (using the ```World``` class from the “just for fun” for Project 3) to make sure that you’re not putting it under the terrain or too far up in the sky.

However, if you want the robot to be affected by physics (similar to how the first-person character could be affected by physics if you used the optional ```CharacterPhysics``` class in Project 3), a class called ```RobotNPC``` has been created to help with this.  RobotNPC.h and RobotNPC.cpp are included in the starter repository, along with World.h and World.cpp. Similar to the ```CharacterPhysics``` class from the previous assignment (if you used it), this class has functions to help the robot stay above the terrain.  Using this class, you can give the robot a target position and the robot will automatically start moving toward that position when you call its ```update()``` function.  Use of the ```RobotNPC``` class is optional and is recommended only if you’re using Project 3 as a starting point for Project 4.  If you do use the ```RobotNPC``` class, you may want that class to be where the scene graph is set up. 

## Submission
Make sure that all of the following are committed to your Git repository in the appropriate directory:
* main.cpp, ofApp.h, and ofApp.cpp
* Any other .h or .cpp files that you created or modified.
* Your vertex and fragment shaders
* Robot / animation design
* Scene graph sketch

**To indicate that all files have been committed and pushed and that your project is ready for grading, submit the URL of the repo to Canvas for the full "Project 4" assignment. (even if it's the same URL as for past projects and/or the check-in for this project).**
